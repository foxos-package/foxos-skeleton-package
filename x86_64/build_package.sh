#!/usr/bin/env bash

## Copyright (C) 2022 SquidRings <squidrings4@gmail.com>
## Everyone is permitted to copy and distribute copies of this file under GNU-GPL3

#repo-add teaching_repo.db.tar.gz *.pkg.tar.zst
repo-add foxos.db.tar.gz *.pkg.tar.zst